class ZhuTakaoka:
    def __init__(self, pattern):
        self.pattern = pattern
        self.shifts = self._compute_shifts(pattern)
        self.hashval = self._compute_hash(pattern)
        self.m = len(pattern)
        
    def _compute_shifts(self, pattern):
        shifts = [0] * len(pattern)
        j = 0
        for i in range(1, len(pattern)):
            while j > 0 and pattern[j] != pattern[i]:
                j = shifts[j-1]
            if pattern[j] == pattern[i]:
                j += 1
            shifts[i] = j
        return shifts
        
    def _compute_hash(self, pattern):
        h = 0
        for i, char in enumerate(pattern):
            h += (ord(char) - 64) * 2**(self.m-i-1)
        return h
        
    def search(self, text):
        n = len(text)
        h = self._compute_hash(text[:self.m])
        matches = []
        for i in range(n - self.m + 1):
            if h == self.hashval and text[i:i+self.m] == self.pattern:
                matches.append(i)
            if i + self.m < n:
                h = (2 * (h - (ord(text[i]) - 64) * 2**(self.m-1)) + 
                     (ord(text[i+self.m]) - 64))
                j = self.shifts[self.m-1]
                while j > 0 and self.pattern[j] != text[i+self.m-1]:
                    j = self.shifts[j-1]
                if self.pattern[j] == text[i+self.m-1]:
                    j += 1
                self.shifts[self.m-1] = j
                for k in range(self.m-2, -1, -1):
                    if self.shifts[k] == 0:
                        self.shifts[k] = self.shifts[k+1]
        return matches
